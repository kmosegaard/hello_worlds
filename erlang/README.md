# "Hello, world!" in Erlang

## How to compile
```
erl hello_world.erl
```

## How to run
```
erl -noshell -s hello_world start -s init stop
```
