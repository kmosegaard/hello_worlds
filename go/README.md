# "Hello, world!" in Go

## How to compile
```
go build hello_world.go
```

## How to run
```
./hello_world
```

OR

```
go run hello_world.go
```
