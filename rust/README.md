# "Hello, world!" in Rust

## How to compile
```
rustc hello_world.rs
```

## How to run
```
./hello_world
```
