# "Hello, world!" in C++

## How to compile
```
g++ hello_world.c -o hello_world
```

## How to run
```
./hello_world
```
