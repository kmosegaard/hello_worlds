# "Hello, world!" in Haskell

## How to compile
```
ghc hello_world.hs -o hello_world
```

## How to run
```
./hello_world
```
